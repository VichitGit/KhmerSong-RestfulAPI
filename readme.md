# Documents Laravel CMS(Content Mangement System)
1. Create Composer 

   https://getcomposer.org/ 
   Make sure your OS(Mac, PC, ...)


2. Project

   2.1 Create Project
   
   ```
   composer create-project --prefer-dist laravel/laravel ProjectName
   ```
   
   Cannot delete or change: laravel/laravel
   
   2.2 Open Service
   
   ```
   php artisan service
   ```
   
   2.3 Config AppServiceProvider
   
   Go to: app->Providers->AppServiceProvider
   
   ```
   public function boot()
    {
        Schema::defaultStringLength(191);
    }
   ```
   
   2.4 Connect to Database
   
   Go to: .evn file and config 
   
   Must create database name in your MySql before laravel connect to it.
   
   ```
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=databaseName
   DB_USERNAME=root
   DB_PASSWORD=password
   ```
   
   2.5 Config Route
   
   Go to: app-> RouteServiceProvider
   
   ```
   protected function mapApiRoutes()
    {
        Route::middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
    ```
    
    2.6 Create Migration with Model
    
    ```
    php artisan make:model ModelName --m
    ```
    
    ## Make Database Schema
     
    ```
      public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id',10);
            $table->string('name',100)->nullalbe();
            $table->string('url',500)->nullalbe();
            $table->integer('categories_id')->unsigned();
            $table->integer('singers_id')->unsigned();
            $table->integer('types_id')->unsigned();
            $table->timestamps();
            
            //RelationShip
            $table->foreign('categories_id')->references('id')->on('categories');
            $table->foreign('singers_id')->references('id')->on('singers');
            $table->foreign('types_id')->references('id')->on('types');
        });
    }
    
   
    public function up()
    {
        Schema::create('singers', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->string('name', 100)->nullalbe();
            $table->string('image', 500)->nullalbe();
            $table->timestamps();
        });
    }
   ```
 
   
   

   
   
          
            
            
     
    
   
  
   
   
  
  
