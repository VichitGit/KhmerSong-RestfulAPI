<?php

namespace App\Http\Controllers\API;

use App\CategoryModel;
use App\Http\Controllers\Controller;


class CategorySongController extends Controller
{

    //$id is id's category
    public function index($id)
    {
        $category = CategoryModel::find($id);
        $songByCategory = $category->song->load('category','singer', 'type');
        return response()->json(['Data' => $songByCategory], 200);
    }
}
