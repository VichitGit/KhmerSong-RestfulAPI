<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\TypeModel;
use function response;

class TypeController extends Controller
{

    public function index()
    {
        $types = TypeModel::has('song')->get();
        return response()->json(['Data' => $types], 201);
    }

    public function show($id)
    {
        $types = TypeModel::has('song')->findOrFail($id);
        return response()->json(['Data' => $types], 201);
    }

}
