<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\SingerModel;
use function response;

class SingerSongController extends Controller
{
    public function index($id)
    {
        $singer = SingerModel::find($id);
        $songOfSinger = $singer->song->load('category','type','singer');

        return response()->json(['Data' => $songOfSinger]);

    }

}
