<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\TypeModel;
use function response;

class TypeSongController extends Controller
{


    public function index($id)
    {
        $type = TypeModel::find($id);
        $songOfTypes = $type->song->load('category','singer', 'type');  
        return response()->json(['Data'=>$songOfTypes]);
    }

}
