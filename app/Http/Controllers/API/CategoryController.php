<?php

namespace App\Http\Controllers\API;

use App\CategoryModel;
use Illuminate\Routing\Controller;
use function response;

class CategoryController extends Controller
{

    public function index()
    {
        //get all category that have in category table
//        $categories = CategoryModel::all();
//        return response()->json(['Data' => $categories], 200);

        //get only category that have relation with primary table
        $category = CategoryModel::has('song')->get();
        return response()->json(['Data' => $category], 200);

    }

    public function show($id)
    {
//        $cate = CategoryModel::findOrFail($id);
//        return response()->json(['Data' => $cate]);

        $category = CategoryModel::has('song')->findOrFail($id);
        return response()->json(['Data' => $category]);


    }

}
