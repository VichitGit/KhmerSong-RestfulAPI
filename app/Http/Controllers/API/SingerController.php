<?php

namespace App\Http\Controllers\API;

use App\SingerModel;
use Illuminate\Routing\Controller;
use function response;

class SingerController extends Controller
{

    public function index()
    {
        //get only category that have relation with primary table
        $singer = SingerModel::has('song')->get();
        return response()->json(['Data' => $singer]);
    }

    public function show($id)
    {
        $singer = SingerModel::has('song')->findOrFail($id);
        return response()->json(['Data' => $singer]);
    }

}
