<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\SongModel;
use function response;

class SongController extends Controller
{

    public function index()
    {
        $song = SongModel::orderBy('id','desc')
            ->get()->load('category', 'singer', 'type');
        return response()->json(["Data" => $song], 201);

        //The same answer
        //$song = $songModel::with('category', 'singer', 'type')->get();
        //return response()->json(["Data" => $song], 201);

        //The same answer
//        $song = SongModel::with('category', 'singer', 'type')->get();
//        return response()->json(['Data' => $song], 201);

    }

    public function show($id)
    {
        $song = SongModel::with('category', 'singer', 'type')->findOrFail($id);
        return response()->json(['data' => $song], 200);

    }

}
