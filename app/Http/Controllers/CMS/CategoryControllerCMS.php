<?php

namespace App\Http\Controllers\CMS;

use App\CategoryModel;
use function compact;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function view;

class CategoryControllerCMS extends Controller
{
    public function index()
    {
        $category = CategoryModel::with('song')->get();
        return view('dashboard.category', compact('category'));
    }

    public function create()
    {
        return view('dashboard.create_category');

    }

    public function store(Request $request)
    {
        $category = new CategoryModel();
        $this->validate($request, [
            'txtCategoryName' => 'required|min:2',
        ]);

        $category->name = $request->txtCategoryName;
        $request->session()->flash('message', '<div class="alert alert-success">Congratulations Insert</div>');
        $category->save();
        return back();
    }

    public function edit($id)
    {
        $category = CategoryModel::get()->find($id);
        return view('detail.update_category', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'txtCategoryName' => 'required|min:2',
        ]);
        $category = CategoryModel::find($id);
        $category->name = $request->txtCategoryName;
        $category->save();
        $request->session()->flash('message', '<div class="alert alert-success">Updated Successfully</div>');
        return redirect('category/');
    }

    public function destroy($id)
    {
        $category = CategoryModel::find($id);
        $category->delete();
        session()->flash('message', '<div class="alert alert-danger">Delete Successfully</div>');
        return redirect('/category');

    }


}
