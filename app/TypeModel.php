<?php

namespace App;

use App\SongModel;
use Illuminate\Database\Eloquent\Model;

class TypeModel extends Model
{
    protected $table = 'types';
    protected $hidden =[
        'created_at',
        'updated_at',
    ];
    public function song()
    {
        return $this->hasMany(SongModel::class,'types_id');
    }
}
