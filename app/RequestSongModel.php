<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestSongModel extends Model
{
    protected $table = 'request_song';
    protected $fillable = [
        'id',
        'song_name',
        'singer_name',
    ];

}
