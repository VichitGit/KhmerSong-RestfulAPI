<?php

namespace App;

use App\CategoryModel;
use App\SingerModel;
use App\TypeModel;
use Illuminate\Database\Eloquent\Model;

class SongModel extends Model
{
    protected $table = 'songs';
    protected $fillable = [
        'id',
        'name',
        'url',
        'categories_id',
        'singers_id',
        'types_id',
    ];


    protected $hidden = [
        'categories_id',
        'singers_id',
        'types_id',
        'created_at',
        'updated_at',
    ];

    public function category()
    {
        //  return $this->belongsTo(CategoryModel::class);
        return $this->belongsTo(CategoryModel::class, 'categories_id');
    }


    public function singer()
    {
        return $this->belongsTo(SingerModel::class, 'singers_id');
    }

    public function type()
    {
        return $this->belongsTo(TypeModel::class, 'types_id');
    }
}
