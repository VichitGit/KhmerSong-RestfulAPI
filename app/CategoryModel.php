<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'id',
        'name',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',

    ];

    public function song()
    {
        return $this->hasMany(SongModel::class, 'categories_id');
    }
}
