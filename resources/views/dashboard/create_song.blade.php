@extends('master_page.master')
@section('content-song')
    <h1 class="text-center">Create New Song</h1>
    <p class="text-center">{!! session('message') !!}</p>

    <Form method="POST" action="{{url('/songs')}}">
        <div class="form-group">
            <input type="text" class="form-control" id="txtSongName" name="txtSongName" placeholder="Name of song">
            <p class="text-danger">{{$errors->first('txtSongName')}}</p>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" id="txtSongURL" name="txtSongURL" placeholder="Update song URL">
            <p class="text-danger">{{$errors->first('txtSongURL')}}</p>
        </div>

        <div class="form-group">
            <select class="form-control" id="cbSingerName" name="cbSingerName">
                @foreach($allSinger as $singer)
                    <option value="{{$singer->id}}">{{$singer->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <div class="form-group">
                <select class="form-control" id="cbCategory" name="cbCategory">
                    @foreach($allCategory as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group-lg">
                <button class="btn btn-danger" type="submit">Save</button>
            </div>
        </div>
    </Form>
@endsection