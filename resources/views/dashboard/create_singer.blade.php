@extends('master_page.master')
@section('content-song')
    <h1 class="text-center">Create New Singer</h1>
    <p class="text-center">{!! session('message') !!}</p>

    <Form method="POST" action="{{url('/singers')}}">
        <div class="form-group">
            <input type="text" class="form-control" id="txtSingerName" name="txtSingerName" placeholder="Name of singer">
            <p class="text-danger">{{$errors->first('txtSingerName')}}</p>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" id="txtSingerImage" name="txtSingerImage" placeholder="Image Singer URL">
            <p class="text-danger">{{$errors->first('txtSingerImage')}}</p>
        </div>
        <div class="form-group-lg">
            <button class="btn btn-danger" type="submit">Save</button>
        </div>

    </Form>
@endsection