@extends('master_page.master')
@section('content-song')
    <h1 class="text-center">Category</h1>
    <p class="text-center">{!! session('message') !!}</p>
    <table class="table table-striped table-inverse">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Action</th>
            <a href="{{'/category/create'}}">
                <button class="btn btn-default">Add New</button>
            </a>


        </tr>
        </thead>
        <tbody>
        @foreach($category as $c)
            <tr>
                <th scope="row">{{ $c->id }}</th>
                <td>{{ $c->name }}</td>
                <td>
                    <button type="button"  class="btn btn-default">
                        <a class="text-primary" href="/category/edit/{{$c->id}}">Update</a>

                    </button>
                </td>
                <td>
                    <form method="POST" action="{{'/category/'.$c->id}}">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-warning">Delete</button>
                    </form>
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
@stop
