@extends('master_page.master')
@section('content-song')
    <h1 class="text-center">All SONGS</h1>
    <p class="text-center">{!! session('message') !!}</p>
    <table class="table table-striped table-inverse">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>URL</th>
            <th>Singer Name</th>
            <th>Category</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($songs as $s)
            <tr>
                <th scope="row">{{ $s->id }}</th>
                <td>{{ $s->name }}</td>
                <td>{{ str_limit($s->url, $limit = 10) }}</td>
                <td>{{ str_limit($s->singer->name, $limit = 10)}}</td>
                <td>{{ $s->category->name}}</td>

                <td>
                    <button type="button" , class="btn btn-default">
                        <a class="text-primary" href="/songs/edit/{{$s->id}}">Update</a>
                    </button>
                </td>
                <td>
                    <form method="POST" action="{{'/songs/'.$s->id}}">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-warning">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="text-center">
        {!! $songs->render() !!}
    </div>

@endsection



