@extends('master_page.master')
@section('content-song')
    <h1 class="text-center">Update Song</h1>
    <p class="text-center">{!! session('message') !!}</p>

    <Form method="POST" action="{{url('songs/'.$songs->id)}}">
        <input name="_method" type="hidden" value="PUT">

        <div class="form-group">
            <input type="text" class="form-control" id="txtSongName" name="txtSongName" placeholder="Update song"
                   value="{{$songs->name}}">
            <p class="text-danger">{{$errors->first('txtSongName')}}</p>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" id="txtSongURL" name="txtSongURL" placeholder="Update song URL"
                   value="{{$songs->url}}">
            <p class="text-danger">{{$errors->first('txtSongURL')}}</p>
        </div>


        <div class="form-group">
            <select class="form-control" id="cbSingerName" name="cbSingerName">
                <option value="{{$songs->singer->id}}">{{$songs->singer->name}}</option>

                @foreach($allSinger as $singer)
                    @if($singer->name != $songs->singer->name)
                        <option value="{{$singer->id}}">{{$singer->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>

        <div class="form-group">

            <div class="form-group">
                <select class="form-control" id="cbCategory" name="cbCategory">
                    <option value="{{$songs->category->id}}">{{$songs->category->name}}</option>

                    @if($songs->category->name == 'Modern')
                        <option value="2">Old</option>
                    @else
                        <option value="1">Modern</option>
                    @endif
                </select>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit" name="submit">Update</button>
            </div>
        </div>

    </Form>

@stop
