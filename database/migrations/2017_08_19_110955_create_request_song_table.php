<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestSongTable extends Migration
{

    public function up()
    {
        Schema::create('request_song', function (Blueprint $table) {
            $table->increments('id');
            $table->string('song_name', 150)->nullable();
            $table->string('singer_name', 50)->nullable();
            $table->string('general_input', 200)->default('false');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('request_song');
    }
}
